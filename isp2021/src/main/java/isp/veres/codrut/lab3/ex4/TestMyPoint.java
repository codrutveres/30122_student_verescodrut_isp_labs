package isp.veres.codrut.lab3.ex4;


public class TestMyPoint {

    public static void main(String[] args) {
        int x;
        int y;

        MyPoint p1 = new MyPoint();
        p1.setX(2);
        p1.setY(4);
        MyPoint p2 = new MyPoint(3,6);

        System.out.println("Point 1: "+p1.toString());
        System.out.println("Point 2: "+p2.toString());

        System.out.println("Distance between Point 1 and Point 2 is: "+ p1.distance(p2));
        System.out.println("Distance between Point 2 and Point (0, 0) is: "+ p2.distance(0,0));
    }
}