package isp.veres.codrut.lab3.ex1;

public class Robot {
    int x;
    public Robot (){
        this.x=1;
    }
    public void change(int k) {
        if (k>=1){
            x = x+k;
        }
    }
    public String toString(){
        return "position: " +x;
    }
}
