package isp.veres.codrut.lab3.ex3;

public class TestAuthor {

    public static void main(String[] args) {
        Author a1 = new Author("David Caleb", "david.caleb@dewin.de", 'M');
        Author a2 = new Author("Darius Frid", "frid.darius@omw.com", 'M');
        Author a3 = new Author("Ion Dascal", "ion.dascal.12@yahoo.ro", 'M');
        Author a4 = new Author("Ana-Maria Oprea", "anaopreamaria@facebook.com", 'F');

        System.out.println("Author 1: "+a1.getName() + " ("+a1.getGender()+")"+ " email at " + a1.getEmail());
        System.out.println("Author 2: "+a2.getName() + " ("+a2.getGender()+")"+ " email at " + a2.getEmail());
        System.out.println("Author 3: "+a3.getName() + " ("+a3.getGender()+")"+ " email at " + a3.getEmail());
        System.out.println("Author 4: "+a4.getName() + " ("+a4.getGender()+")"+ " email at " + a4.getEmail());
    }
}