package isp.veres.codrut.lab2;

public class Exercise6 {
    public static int nonRecursive(int n){
        int fact=1;
        for (int i=1;i<=n;i++) {
            fact = fact * i;
        }
        return fact;
    }
    public static int recursive(int n){
        if (n==1){
            return 1;
        }
        else {
            return n*recursive(n-1);
        }
    }

    public static void main(String[] args) {
        System.out.println("N!(nerecursiv) = "+ nonRecursive(5));
        System.out.println("N!(recursiv) = "+ recursive(5));
    }
}