package isp.veres.codrut.lab2;

public class Exercise4 {
    public static void maxInVector (int[] array){
        int max=0;
        for (int i=0;i<array.length;i++) {
            if (max<array[i]){
                max=array[i];
            }
        }
        System.out.println("The max is: "+max);
    }

    public static void main(String[] args) {
        int  [] vector = {4,2,6,8,10,11};
        maxInVector(vector);
        System.out.println(vector);
    }
}