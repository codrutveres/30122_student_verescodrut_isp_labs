package isp.veres.codrut.lab2;

public class Exercise3 {
    public static boolean isPrim(int n){
        if (n<= 1) {
            return false;
        }
        for (int i = 2; i< n; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static void primesAB(int A,int B) {
        for (int i = A; i <= B; i++) {
            if (isPrim(i)) {
                System.out.println(i);
            }
        }
    }

    public static void main(String[] args) {
        primesAB(3,17);
    }
}
