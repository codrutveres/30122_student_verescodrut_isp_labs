package isp.veres.codrut.lab2;

import java.util.Arrays;
import java.util.Scanner;

public class Exercise5 {
    public static void BubbleSort (int[] array) {
        boolean sorted = false;
        int temp;
        while (!sorted) {
            sorted = true;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i+1]) {
                    temp = array[i];
                    array[i] = array[i+1];
                    array[i+1] = temp;
                    sorted = false;
                }
            }
        }
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the length of the array:");
        int length = in.nextInt();
        int[] vector = new int[length];
        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < length; i++) {
            vector[i] = in.nextInt();
        }
        System.out.println(Arrays.toString(vector));
        BubbleSort(vector);
        System.out.println("The array after sorting.");
        System.out.println(Arrays.toString(vector));
    }
}
