package isp.veres.codrut.lab2;

import java.util.Random;
import java.util.Scanner;

public class Exercise7 {
    public static void guess(int number){
        int tries=3;
        while (tries != 0){
            Scanner in = new Scanner(System.in);
            System.out.print("Your guess: ");
            int answer = in.nextInt();
            if (answer==number){
                System.out.println("You win");
                System.exit(1);
            }
            if (answer<number){
                System.out.println("Wrong answer,your number is too low");
                tries--;
                System.out.println("You have "+tries+ " more tries.");
            }
            if (answer>number){
                System.out.println("Wrong answer,your number is too high");
                tries--;
                System.out.println("You have "+tries+ " more tries.");
            }
        }
        System.out.println("The number was "+number);
        System.out.println("You lost!");
    }

    public static int randgen(){
        Random rand =new Random();
        int number = rand.nextInt(10);
        return number;
    }

    public static void main(String[] args) {
        int rand_nr= randgen();
        guess(rand_nr);
    }
}
