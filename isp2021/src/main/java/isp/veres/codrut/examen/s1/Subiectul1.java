package isp.veres.codrut.examen.s1;

import java.util.ArrayList;

public class Subiectul1 {

    public class U {
        public void p() {

        }

    }

    public class J {


    }

    public class I extends U {
        private long t;

        public void f() {

        }

        public void i(J j){

        }

    }


    public class K {

        private ArrayListM arrayListM;
        private L[] arrayL;


        public K() {
            this.arrayListM = new ArrayListM();
            this.arrayL = new L[100];
        }
    }


    public class L {

        public void metA() {

        }
    }

    public class N {
        private I i;
    }

    public class S {
        public void metB() {

        }
    }

}
