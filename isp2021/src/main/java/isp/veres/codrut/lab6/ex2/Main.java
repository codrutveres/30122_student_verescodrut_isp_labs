package isp.veres.codrut.lab6.ex2;

public class Main {
    public static void main(String[] args) {

        Bank a = new Bank();
        a.addAccount("Daniela", 600);
        a.addAccount("Alexandra", 500);
        a.addAccount("Alina", 800);
        a.printAccounts();
    }
}
