package isp.veres.codrut.lab6.ex2;

import java.util.Objects;

public class BankAccount implements Comparable<BankAccount> {
    private String owner;
    private double balance;

    public void withdraw(double amount) {
        balance = balance - amount;
    }

    public void deposit(double amount) {
        balance = balance + amount;
    }

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    public String getOwner() {

        return owner;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "owner='" + owner + '\'' +
                ", balance=" + balance +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankAccount that = (BankAccount) o;
        return Double.compare(that.balance, balance) == 0 && Objects.equals(owner, that.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, balance);
    }

    @Override
    public int compareTo(BankAccount o) {
        if (o.getBalance() > balance) {
            return -1;
        } else if (o.getBalance() < balance) {
            return 1;
        } else {
            return 0;
        }

    }
}
