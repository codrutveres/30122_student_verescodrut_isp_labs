package isp.veres.codrut.lab6.ex3;

import java.util.Objects;

public class BankAccount {
    private String owner;
    private double balance;

    public void withdraw(double amound) {
        balance = balance - amound;

    }

    public void deposit(double amound) {
        balance = balance + amound;
    }

    public BankAccount(String owner, double balance) {
        this.owner=owner;
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    public String getOwner() {
        return owner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankAccount that = (BankAccount) o;
        return Double.compare(that.balance, balance) == 0 && Objects.equals(owner, that.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, balance);
    }
}
