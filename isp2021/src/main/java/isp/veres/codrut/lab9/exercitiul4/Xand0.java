package isp.veres.codrut.lab9.exercitiul4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Xand0 extends JPanel {

    char playerMark = 'X';
    JButton[] buttons = new JButton[9];

    public Xand0() {
        setLayout(new GridLayout(3, 3));
        initializeButtons();
    }

    public void initializeButtons() {
        for (int i = 0; i <= 8; i++) {
            buttons[i] = new JButton();
            buttons[i].setText("-");
            buttons[i].setBackground(Color.WHITE);
            buttons[i].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JButton clicked = (JButton) e.getSource();
                    clicked.setText(String.valueOf(playerMark));
                    if (playerMark == 'X') {
                        playerMark = '0';
                        clicked.setBackground(Color.RED);
                    } else {
                        playerMark = 'X';
                        clicked.setBackground(Color.CYAN);
                    }
                    displayWinner();
                }
            });
            add(buttons[i]);
        }
    }

    public void displayWinner(){
        if(checkWinner() == true){
            if(playerMark == 'X'){
                playerMark = '0';
            }else playerMark = 'X';
            JOptionPane pane = new JOptionPane();
            int dialogResult = JOptionPane.showConfirmDialog(pane, "Game Over. "+ playerMark + " wins. Would you like to play again?","Game over.",
                    JOptionPane.YES_NO_OPTION);

            if(dialogResult == JOptionPane.YES_OPTION) resetTheButtons();
            else System.exit(0);
        }
        else if(checkDraw()) {
            JOptionPane pane = new JOptionPane();
            int dialogResult = JOptionPane.showConfirmDialog(pane, "Draw. Play again?","Game over.", JOptionPane.YES_NO_OPTION);
            if(dialogResult == JOptionPane.YES_OPTION)  resetTheButtons();
            else System.exit(0);
        }
    }

    public void resetTheButtons(){
        playerMark = 'X';
        for (int i = 0; i < 9; i++) {
            buttons[i].setText("-");
            buttons[i].setBackground(Color.WHITE);
        }
    }

    public boolean checkWinner(){
        if(checkRows() == true || checkColumns() == true || checkDiagonals() == true){
            return true;
        }
        return false;
    }

    public boolean checkRows(){
        int i = 0;
        for (int j = 0; j < 3; j++) {
            if(buttons[i].getText().equals(buttons[i+1].getText()) && buttons[i].getText().equals(buttons[i+2].getText())
            && buttons[i].getText().charAt(0) != '-'){
                return true;
            }
            i = i+3;
        }
        return false;
    }

    public boolean checkColumns(){
        int i = 0;
        for (int j = 0; j < 3; j++) {
            if(buttons[i].getText().equals(buttons[i+3].getText()) && buttons[i].getText().equals(buttons[i+6].getText())
                    && buttons[i].getText().charAt(0) != '-'){
                return true;
            }
            i = i+1;
        }
        return false;
    }

    public boolean checkDiagonals(){
        if((buttons[0].getText().equals(buttons[4].getText())) && (buttons[0].getText().equals(buttons[8].getText()))
        && (buttons[0].getText().charAt(0) != '-')){
            return true;
        }else if((buttons[2].getText().equals(buttons[4].getText())) && (buttons[2].getText().equals(buttons[6].getText()))
                && (buttons[2].getText().charAt(0) != '-')){
            return true;
        }
        return false;
    }

    public boolean checkDraw(){
        boolean full = true;
        for (int i = 0; i < 9; i++) {
            if(buttons[i].getText().charAt(0) == '-'){
                full = false;
            }
        }
        return full;
    }

}
