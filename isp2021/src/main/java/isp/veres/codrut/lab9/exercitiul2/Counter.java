package isp.veres.codrut.lab9.exercitiul2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Counter extends JFrame {

    JTextField tCounter;
    JButton bCounter;

    Counter() {
        setTitle("Exercise 1");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400, 500);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);

        tCounter = new JTextField();
        tCounter.setBounds(150, 50, 50, 50);

        bCounter = new JButton("Count");
        bCounter.setBounds(150, 300, 100, 100);
        bCounter.addActionListener(new TratareButonCount());

        add(tCounter);
        add(bCounter);
    }

    public class TratareButonCount implements ActionListener {
        int k = 0;

        @Override
        public void actionPerformed(ActionEvent e) {
            k++;
            String str = Integer.toString(k);
            tCounter.setText(str);
        }

    }

}
