package isp.veres.codrut.lab9.exercitiul5;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Window extends JFrame {

    JTextField textField,addField;
    JButton button;

    Window() {
        setTitle("Exercise 2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(500, 500);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);

        textField = new JTextField();
        textField.setBounds(10,10,470,300);
        add(textField);

        addField = new JTextField();
        addField.setBounds(10,310,470,70);
        add(addField);

        button = new JButton("Add a Train");
        button.setBounds(20,400,100,50);
        add(button);

    }

    public class AListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            String userString = Window.this.addField.getText();
            Controler controler = new Controler(userString);
            textField.setText(userString);

        }
    }

}
