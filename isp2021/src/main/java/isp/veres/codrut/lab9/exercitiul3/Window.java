package isp.veres.codrut.lab9.exercitiul3;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class Window extends JFrame {

    JTextField textField;
    JButton button;
    JTextArea textArea;

    Window() {
        setTitle("Exercise 2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400, 500);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);

        textField = new JTextField();
        textField.setBounds(10, 10, 300, 20);
        add(textField);

        textArea = new JTextArea("Choose a file and write the name of the file in the text\n" +
                "field above\n" +
                "Files available for display are:\n" +
                "test.txt\n" +
                "hello.txt");
        textArea.setBounds(10, 40, 300, 200);
        add(textArea);

        button = new JButton("Display");
        button.setBounds(100, 300, 100, 100);
        button.addActionListener(new TratareButon());
        add(button);

    }

    public class TratareButon implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            String userString = Window.this.textField.getText();
            if(userString.equals("test.txt")){
               textArea.setText("");
                try {
                    BufferedReader bf = new BufferedReader(new FileReader("C:\\Users\\codru\\Desktop\\lab\\30122_student_verescodrut_isp_labs\\isp2021\\src\\main\\java\\isp\\veres\\codrut\\lab9\\exercitiul3\\test.txt"));
                    String l = "";
                    l = bf.readLine();
                    while(l!=null){
                        textArea.append(l+"\n");
                        l = bf.readLine();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else if(userString.equals("hello.txt")) {
                textArea.setText("");
                try {
                    BufferedReader bf = new BufferedReader(new FileReader("C:\\Users\\codru\\Desktop\\lab\\30122_student_verescodrut_isp_labs\\isp2021\\src\\main\\java\\isp\\veres\\codrut\\lab9\\exercitiul3\\hello.txt"));
                    String l = "";
                    l = bf.readLine();
                    while (l != null) {
                        textArea.append(l + "\n");
                        l = bf.readLine();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else {
                textArea.setText("File was not found");
            }

        }
    }
}
