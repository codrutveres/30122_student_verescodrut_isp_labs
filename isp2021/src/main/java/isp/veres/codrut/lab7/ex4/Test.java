package isp.veres.codrut.lab7.ex4;

import java.io.IOException;

public class Test {


    public static void main(String[] args) throws IOException, ClassNotFoundException {

        Car c1 = new Car("BMW",5000);
        Car c2 = new Car();
        CarMaker cm1 = new CarMaker();
        cm1.createCar("Mercedes",4500);
        cm1.saveCar(c1,"out.txt");
        cm1.saveCar(c2,"out.txt");
        cm1.readCar("out.txt");

    }
}