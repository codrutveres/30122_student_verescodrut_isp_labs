package isp.veres.codrut.lab7.ex2;

import java.io.*;

public class CountService {

    public static void readFile() throws FileNotFoundException {
        String path = "C:\\Users\\codru\\Desktop\\lab\\30122_student_verescodrut_isp_labs\\isp2021\\src\\main\\java\\isp\\veres\\codrut\\lab7\\ex2\\data.txt";
        String line = "";
        int count = 0;
        try{

            BufferedReader br = new BufferedReader(new FileReader(path));
            while ((line = br.readLine()) != null){
                System.out.println(line);
                for (int i = 0; i < line.length(); i++) {
                    if(line.charAt(i) == 'e'){
                        count++;
                    }
                }
            }
        }catch (FileNotFoundException e){
            System.out.println("File not found");
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Number of 'e' appears is: "+count);
    }

}
