package isp.veres.codrut.lab7.ex1;

public class CofeeNrException extends Exception{


    int n;
    public CofeeNrException(int n,String msg){
        super(msg);
        this.n = n;
    }

    int getNumber(){
        return n;
    }

}
