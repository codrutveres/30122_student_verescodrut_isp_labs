package isp.veres.codrut.lab5.ex1;

public abstract class Shape {
    protected String color;
    protected boolean filled;

    abstract double getArea();
    abstract double getPerimeter();

    public Shape(){
        color = "red";
        filled = false;
    }

    public Shape(String color, boolean filled){
        this.color = color;
        this.filled = filled;
    }

    public String getColor(){
        return color;
    }

    public void setColor(String color){
        this.color = color;
    }

    public boolean isFilled(){
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public String toString(){
        if (isFilled() == true)
            return("A Shape colored " + getColor() + " and is filled.");
        else
            return("A Shape colored " + getColor() + " and is not filled.");
    }
}
