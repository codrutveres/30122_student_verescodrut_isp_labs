package isp.veres.codrut.lab5.ex1;

public class Main {
    public static void main(String[] args) {
        Shape[] shapes = {new Circle(), new Rectangle(), new Square()};
        for (Shape shape: shapes
        ) {
            System.out.println("Area is " + shape.getArea());
            System.out.println("Perimeter is " + shape.getPerimeter());
            System.out.println(shape.toString());
            System.out.println();
        }
    }
}