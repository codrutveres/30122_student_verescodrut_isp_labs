package isp.veres.codrut.lab5.ex4;

import java.util.Random;

public class TemperatureSensor extends Sensor{
    @Override
    public int readValue() {
        Random rand = new Random();
        int randomNumber = rand.nextInt(100);
        return(randomNumber);
    }
}