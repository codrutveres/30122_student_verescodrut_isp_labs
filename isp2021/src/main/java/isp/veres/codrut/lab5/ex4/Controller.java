package isp.veres.codrut.lab5.ex4;

public class Controller{
    public void control(){
        TemperatureSensor temperature = null;
        LightSensor light = null;

        synchronized (Controller.class){
            if(temperature == null || light == null){
                temperature = new TemperatureSensor();
                light = new LightSensor();
            }
        }
        for(int i=0; i<20; i++){
            System.out.println("Temperature: " + temperature.readValue() + " Light: " + light.readValue());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }
}
