package isp.veres.codrut.colocviu;

public class Robot {

    int id;
    String brand;
    String engine;
    int arms;

    public Robot(int id,String brand,String engine,int arms){

        this.brand = brand;
        this.id = id;
        this.engine = engine;
        this.arms = arms;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getArms() {
        return arms;
    }

    public void setArms(int arms) {
        this.arms = arms;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }
}
