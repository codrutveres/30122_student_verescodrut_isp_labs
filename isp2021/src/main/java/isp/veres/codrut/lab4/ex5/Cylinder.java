package isp.veres.codrut.lab4.ex5;

public class Cylinder extends Circle{
    private double height = 1.0;

    public Cylinder(){
        this.height = 1.0;
    }

    public Cylinder(double radius){

        super(radius);
    }

    public Cylinder (double radius , double height){
        this(radius);
        this.height = height;
    }

    public double getHeight(){

        return this.height;
    }

    public double getVolume() {
        return super.getArea()*height;
    }

    public double getArea() {
        return 2*3.14*super.getRadius()*(this.height + super.getRadius());
    }
}
