package isp.veres.codrut.lab4.ex6;
public class Shape {
    String  color;
    boolean filled;

    public Shape() {
        this.color="green";
        this.filled= true;

    }

    public Shape (String color, boolean filled) {
        this.color  = color;
        this.filled = filled;
    }

    @Override
    public String toString() {
        if(this.filled)
            return "A Shape with color of " + this.color + "and filled";
        else
            return "A shape with color of " +this.color + "and not filled";

    }
}

