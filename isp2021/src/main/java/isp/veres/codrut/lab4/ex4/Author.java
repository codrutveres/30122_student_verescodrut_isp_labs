package isp.veres.codrut.lab4.ex4;

public class Author {
    private String name;
    private String email;
    private char gender;


    public Author(String name, String email, char gender) {
        this.name = name;
        this.email = email;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public char getGender() {
        return gender;
    }

    public String toString() {
        return ("The Author name is " + name + " the email is " + email + "the gender is " + gender);
    }
}
