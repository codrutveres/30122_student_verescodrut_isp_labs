package isp.veres.codrut.lab4.ex6;

public class Circle extends Shape{
    double radius;
    boolean failed;

    public Circle (double radius, String color, boolean failed) {
        this.color=color;
        this.radius=radius;
        this.failed=failed;
    }


    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return 3.14 * Math.pow(getRadius(),2);
    }
    public double getPerimeter() {
        return 2 * 3.14 * getRadius();
    }

    @Override
    public String toString() {
        return "A Circle with radius=" + radius + " which is a subclass of "+ super.toString() ;
    }
}
