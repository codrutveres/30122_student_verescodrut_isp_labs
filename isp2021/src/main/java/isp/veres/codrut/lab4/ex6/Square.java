package isp.veres.codrut.lab4.ex6;

public class Square extends Rectangle {

    public Square(double side, String color, boolean filled) {

        super(side, side, color, filled);
    }

    public void setWidth(double side) {
        super.setWidth(side);
    }


    public void setLength(double side) {
        super.setLength(side);
    }

    public String toString() {
        return "A Square with side = " + width + ", which is a subclass of " + super.toString();
    }
}
