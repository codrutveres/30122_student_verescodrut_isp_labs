package isp.veres.codrut.lab4.ex4;

public class Main {
    public static void main(String[] args) {
        Author[] authors = new Author[2];
        authors[0]=new Author("Popescu David","popescudavid@gmail.com",'m');
        authors[1]=new Author("Iliescu Sabina", "iliescusabina@gmail.com",'f');
        Book book=new Book("Basme",authors,20,3000);
        System.out.println(book);
        System.out.println(authors[0].getName()+ " Si "+authors[1].getName());
    }
}
