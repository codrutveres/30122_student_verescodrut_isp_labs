package isp.veres.codrut.lab4.ex5;

public class Circle {
    private double radius = 1.0;
    private String color = "red";

    Circle() {
        radius = 0;
        color = "";

    }
    Circle (double radius){
        this.radius = radius;
    }

    public double getRadius () {
        return radius;}

    public double getArea() {

        return radius * radius * Math.PI;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                ", color='" + color + '\'' +
                '}';
    }
}
