package isp.veres.codrut.lab10.exercitiul6;

import javax.swing.*;

public class MyThread extends Thread {

    JTextArea textArea = new JTextArea();

    public MyThread(JTextArea textArea) {
        this.textArea = textArea;
    }

    public void run() {
        synchronized (textArea) {
            int sec = 0;
            int min = 0;
            int hours = 0;
            String s = "0", s2 = "0", s3 = "0";
            for (sec = 0; sec <= 60; sec++) {
                s = String.valueOf(sec);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (sec == 60) {
                    sec = 0;
                    min++;
                    s2 = String.valueOf(min);
                }
                if (min == 60) {
                    min = 0;
                    hours++;
                    s3 = String.valueOf(hours);
                }
                textArea.setText(s3 + ":" + s2 + ":" + s);
            }

        }
    }

}

