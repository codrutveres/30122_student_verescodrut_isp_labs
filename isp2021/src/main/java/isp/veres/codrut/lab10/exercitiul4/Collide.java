package isp.veres.codrut.lab10.exercitiul4;

public class Collide extends Thread{

    Robot r1,r2;

    public Collide(Robot r1,Robot r2){
        this.r1=r1;
        this.r2=r2;
    }


    public void run(){
        if(r1.x==r2.x && r1.y==r2.y){
            System.out.println("2 Robots COLLIDE");
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
