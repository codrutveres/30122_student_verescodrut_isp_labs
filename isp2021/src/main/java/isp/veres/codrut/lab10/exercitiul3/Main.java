package isp.veres.codrut.lab10.exercitiul3;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        MyThread t1 = new MyThread();

        MyRunnable r1 = new MyRunnable();
        Thread t2 = new Thread(r1);


        t1.start();
        t1.join();
        t2.start();

    }

}
