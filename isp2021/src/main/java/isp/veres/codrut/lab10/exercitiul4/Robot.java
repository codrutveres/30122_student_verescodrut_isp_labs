package isp.veres.codrut.lab10.exercitiul4;

import java.util.Random;

public class Robot extends Thread {

    int x;
    int y;
    boolean c = false;
    private Random random = new Random();

    public Robot(String name, int x, int y) {
        super(name);
        this.x = x;
        this.y = y;
    }

    @Override
    public void run() {
        while (!c) {

            System.out.println("Robot " + getName() + " coordinates are: (" + this.x + " ," + this.y + ")");
            int a = random.nextInt(5);

            switch (a) {
                case 1:
                    x++;
                    break;
                case 2:
                    x--;
                    break;
                case 3:
                    y++;
                    break;
                case 4:
                    y--;
                    break;
            }

            if (x < 0 || y < 0 || x > 6 || y > 6) {
                c = true;
            }

        }
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
