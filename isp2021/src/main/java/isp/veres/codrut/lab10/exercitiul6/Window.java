package isp.veres.codrut.lab10.exercitiul6;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Window extends JFrame {

    JButton button1;
    JButton button2;
    JTextArea textArea;


    Window() {
        setTitle("Chronometer");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400, 500);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);

        textArea = new JTextArea("0:0:0");
        textArea.setBounds(100,100,200,50);
        add(textArea);

        button1 = new JButton("START/PAUSE");
        button1.setBounds(100,200,200,50);
        button1.addActionListener(new tratareButon1());
        add(button1);

        button2 = new JButton("STOP");
        button2.setBounds(100,260,200,50);
        button2.addActionListener(new tratareButon2());
        add(button2);
    }

    public class tratareButon1 implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            MyThread t = new MyThread(textArea);
            t.start();

        }
    }

    public class tratareButon2 implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {



        }
    }
}
